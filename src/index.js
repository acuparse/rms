/**
 * Acuparse Release Management Service
 * @copyright Copyright (C) 2015-2022 Maxwell Power
 * @author Maxwell Power <max@acuparse.com>
 * @link http://www.acuparse.com
 * @license MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * File: index.js
 * Main Application
 */

const port = process.env.PORT || 8080;
const dbHostname = process.env.DATABASE_HOSTNAME || 'localhost';
const dbPort = process.env.DATABASE_PORT || '27017';
const dbName = process.env.DATABASE_NAME || 'telemetry'

// Import Required Modules
const compression = require('compression');
const Ddos = require('ddos')
const express = require('express');
const ddos = new Ddos({burst: 5, limit: 10})
const bearerToken = require('express-bearer-token');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
require('log-timestamp');

// Initialise the app
const app = express();
app.disable('x-powered-by');
app.use(ddos.express);
app.use(compression())
app.use(bearerToken());

// Configure bodyparser to handle post requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// Default URL
app.all('/', (req, res) => res.sendFile(path.join(__dirname, 'html', 'index.html')));

// Releases Route
app.use('/current', require("./routes/clients"));

// Client View Route
app.use('/view', require("./routes/viewClient"));

// User Management Route
app.use('/users', require("./routes/manageUsers"));

// Release Management Route
app.use('/releases', require("./routes/manageReleases"));

// Client Management Route
app.use('/manage', require("./routes/manageClients"));

// Total Count
app.use('/count', require("./routes/totalCount"));

// Healthcheck
app.use('/healthcheck', require('express-healthcheck')());

// Memory Usage
app.get('/sysmon', async function (req, res) {
    const auth = require('./includes/authenticate');
    if (await auth.authenticate(req.token)) {
        res.json({
            memory: Math.round(process.memoryUsage().heapUsed / (1024 * 1024)) + ' MB',
            cpu: process.cpuUsage(),
            version: process.version
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
});

// Everything else
app.all('/*', function (req, res) {
    res.status(404).json({status: 'ERROR', message: 'Not Found'})
});

// Connect to Mongoose
async function dbConnect() {
    await mongoose.connect('mongodb://' + dbHostname + ':' + dbPort + '/' + dbName, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
}

// Launch
async function startService() {
    await app.listen(port);
}

dbConnect()
    .then(r => startService()
            .then(r => console.log('Running Acuparse Release Management Service on port ' + port))
            .catch(r => console.ERROR('Failed to Start Acuparse Release Management Service')),
        console.log('Successfully Connected to Database'))
    .catch(r => console.ERROR('Database Connection Failed!'));

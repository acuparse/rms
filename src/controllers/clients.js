/**
 * Acuparse Release Management Service
 * @copyright Copyright (C) 2015-2022 Maxwell Power
 * @author Maxwell Power <max@acuparse.com>
 * @link http://www.acuparse.com
 * @license MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * File: controllers/clients.js
 * Public API Controller
 */

// Import Database
DB_secrets = require('../models/secrets');
DB_clients = require('../models/clients');
DB_releases = require('../models/releases');

// Import Auth
const auth = require('../includes/authenticate');

function latestRelease(callback) {
    DB_releases.findOne({}, {}, {sort: {released_on: -1}}, function (err, releases) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, releases);
        }
    });
}

// Index
exports.index = async function (req, res) {
    latestRelease(async function (err, release) {
        if (err) {
            console.log(err);
        }
        if (await auth.authenticate(req.token)) {
            if (err)
                res.json(err);
            res.json({
                latestRelease: release.release
            });
            console.log('ADMIN: Viewed Latest Release')
        } else {
            res.json({
                status: 'ERROR',
                message: 'Unauthorized'
            })
        }
    });
}

// Process Update Check
exports.checkUpdate = async function (req, res) {
    latestRelease(async function (err, release) {
        if (err) {
            console.log(err);
        }
        if (req.body.clientID) {
            if (req.body.clientID.length === 41) {
                if (req.body.mac.length >= 12) {
                    const clients = new DB_clients();
                    await DB_clients.exists({'clientID': req.body.clientID}, async function (err, result) {
                        if (result) {
                            await DB_clients.findOne({'clientID': req.body.clientID}, async function (err, clients) {
                                if (err)
                                    res.send(err);
                                clients.version = req.body.version;
                                clients.chassis = req.body.chassis;
                                clients.virt = req.body.virtualization;
                                clients.kernel = req.body.kernel;
                                clients.os = req.body.operating_system;
                                clients.arch = req.body.architecture;
                                clients.mac = req.body.mac;
                                clients.updated_on = new Date();
                                await clients.save(function (err) {
                                    if (err)
                                        res.json(err);
                                    res.json({
                                        status: 'SUCCESS',
                                        message: 'Updated Install',
                                        latestRelease: release.release
                                    });
                                    console.log('Client ' + req.body.clientID + ' Updated')
                                });
                            });
                        } else {
                            clients.clientID = req.body.clientID ? req.body.clientID : clients.clientID;
                            clients.version = req.body.version;
                            clients.chassis = req.body.chassis;
                            clients.virt = req.body.virtualization;
                            clients.kernel = req.body.kernel;
                            clients.os = req.body.operating_system;
                            clients.arch = req.body.architecture;
                            clients.mac = req.body.mac;
                            await clients.save(function (err) {
                                if (err)
                                    res.send(err);
                                res.json({
                                    status: 'SUCCESS',
                                    message: 'Created Install',
                                    latestRelease: release.release
                                });
                                console.log('Install ' + req.body.clientID + ' Added')
                            });
                        }
                    });
                } else {
                    res.json({
                        status: 'ERROR',
                        message: 'Invalid MAC'
                    })
                }
            } else {
                res.json({
                    status: 'ERROR',
                    message: 'Invalid clientID'
                })
            }
        } else {
            res.json({
                status: 'ERROR',
                message: 'Missing clientID'
            })
        }
    });
}

// View One
exports.view = async function (req, res) {
    await DB_clients.findOne({'clientID': req.params.client}, '-_id -__v -clientID', function (err, client) {
            if (err)
                res.send(err);
            if (client) {
                res.json({
                    client
                });
            } else {
                res.json({
                    status: 'ERROR',
                    message: 'clientID Not Found'
                });
            }
        }
    );
}

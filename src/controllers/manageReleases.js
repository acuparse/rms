/**
 * Acuparse Release Management Service
 * @copyright Copyright (C) 2015-2022 Maxwell Power
 * @author Maxwell Power <max@acuparse.com>
 * @link http://www.acuparse.com
 * @license MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * File: controllers/manageClients.js
 * Admin API Controller
 */

// Import Databases
DB_secrets = require('../models/secrets');
DB_releases = require('../models/releases');

const auth = require('../includes/authenticate');

// Add Release
exports.addRelease = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_releases.exists({'release': req.body.version}, function (err, result) {
            if (!result) {
                const releases = new DB_releases();
                releases.release = req.body.version;
                releases.description = req.body.description;
                releases.save(function (err) {
                    if (err)
                        res.json(err);
                    res.status(201).send({
                        status: 'SUCCESS',
                        message: 'Added Release',
                        releases
                    });
                    console.log('[ADMIN] Added Version ' + req.body.version);
                });
            } else {
                res.status(409).send({
                        status: 'ERROR',
                        message: 'Release Exists'
                    }
                );
            }
        })
    } else {
        res.status(401).send({
                status: 'ERROR',
                message: 'Unauthorized'
            }
        );
    }
}

// List All Releases
exports.listReleases = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_releases.find(function (err, releases) {
            if (err) {
                res.json({
                    status: 'ERROR',
                    message: err,
                });
            }
            res.json({
                releases
            });
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// Delete release
exports.deleteRelease = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_releases.exists({'release': req.body.version}, function (err, result) {
            if (result) {
                DB_releases.deleteMany({'release': req.body.version}, function (err) {
                    if (err)
                        res.send(err);
                    res.json({
                        status: 'SUCCESS',
                        message: 'Removed Release'
                    });
                    console.log('[ADMIN] Removed Version ' + req.body.version);
                });
            } else {
                res.status(400).send({
                        status: 'ERROR',
                        message: 'Release Does Not Exist'
                    }
                );
            }
        });
    } else {
        res.status(401).send({
                status: 'ERROR',
                message: 'Unauthorized'
            }
        );
    }
}

// View Release Count
exports.releaseCount = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_releases.estimatedDocumentCount(function (err, count) {
            if (err) {
                res.json({
                    status: 'ERROR',
                    message: err,
                });
            }
            res.json({
                totalReleases: count
            });
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

/**
 * Acuparse Release Management Service
 * @copyright Copyright (C) 2015-2022 Maxwell Power
 * @author Maxwell Power <max@acuparse.com>
 * @link http://www.acuparse.com
 * @license MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * File: controllers/manageClients.js
 * Client/Install Admin Tasks
 */

// Import Databases
DB_secrets = require('../models/secrets');
DB_clients = require('../models/clients');

// Import Auth
const auth = require('../includes/authenticate');

// View One Client
exports.view = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_clients.findOne({'clientID': req.body.client}, function (err, clients) {
            if (err)
                res.send(err);
            res.json({
                clients
            });
        })
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// View by MAC
exports.viewMAC = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_clients.find({'mac': req.body.mac}, function (err, clients) {
            if (err)
                res.send(err);
            res.json({
                clients
            });
        })
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// List All
exports.list = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_clients.find({}, {}, {sort: {updated_on: -1}}, function (err, clients) {
            if (err) {
                res.json({
                    status: 'ERROR',
                    message: err,
                });
            }
            res.json({
                clients
            });
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// List Active
exports.listActive = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        let search = new Date(new Date().getTime() - (48 * 60 * 60 * 1000));
        await DB_clients.find({updated_on: {$gte: search}}, function (err, clients) {
            if (err) {
                res.json({
                    status: 'ERROR',
                    message: err,
                });
            }
            res.json({
                clients
            });
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// Update
exports.update = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_clients.findOne({'clientID': req.body.clientID}, async function (err, clients) {
            if (err)
                res.send(err);
            clients.version = req.body.version;
            clients.chassis = req.body.chassis;
            clients.virt = req.body.virtualization;
            clients.kernel = req.body.kernel;
            clients.os = req.body.operating_system;
            clients.arch = req.body.architecture;
            clients.mac = req.body.mac;
            clients.updated_on = new Date();
            await clients.save(function (err) {
                if (err)
                    res.json(err);
                res.json({
                    status: 'SUCCESS',
                    message: 'Updated Install'
                });
                console.log('[ADMIN] Client ' + req.body.clientID + ' Updated');
            });
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// Delete
exports.delete = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_clients.exists({'clientID': req.body.client}, function (err, result) {
            if (result) {
                DB_clients.deleteMany({'clientID': req.body.client}, function (err) {
                    if (err)
                        res.send(err);
                    res.json({
                        status: 'SUCCESS',
                        message: 'client removed'
                    });
                    console.log('[ADMIN] Client ' + req.body.clientID + ' Deleted');
                });
            } else {
                res.status(400).send({
                        status: 'ERROR',
                        message: 'clientID Does Not Exist'
                    }
                );
            }
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// Delete MAC
exports.deleteMAC = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_clients.exists({'mac': req.body.mac}, function (err, result) {
            if (result) {
                DB_clients.deleteMany({'mac': req.body.mac}, function (err) {
                    if (err)
                        res.send(err);
                    res.json({
                        status: 'SUCCESS',
                        message: 'mac removed'
                    });
                    console.log('[ADMIN] MAC ' + req.body.mac + ' Deleted');
                });
            } else {
                res.status(400).send({
                        status: 'ERROR',
                        message: 'MAC Does Not Exist'
                    }
                );
            }
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// Delete Old
exports.deleteOLD = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        let search = new Date(new Date().getTime() - (48 * 60 * 60 * 1000));
        DB_clients.deleteMany({updated_on: {$lt: search}}, function (err) {
            if (err)
                res.send(err);
            res.json({
                status: 'SUCCESS',
                message: 'Stale Installs Removed'
            });
            console.log('[ADMIN] Cleaned Stale Installs');
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// Delete All
exports.deleteALL = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        DB_clients.deleteMany(function (err) {
            if (err)
                res.send(err);
            res.json({
                status: 'SUCCESS',
                message: 'ALL Installs Removed'
            });
            console.log('[ADMIN] Cleaned ALL Installs');
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// View Total Count
exports.count = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_clients.estimatedDocumentCount(function (err, count) {
            if (err) {
                res.json({
                    status: 'ERROR',
                    message: err,
                });
            }
            res.json({
                totalClients: count
            });
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// View Active Count
exports.countActive = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        let search = new Date(new Date().getTime() - (48 * 60 * 60 * 1000));
        await DB_clients.countDocuments({'updated_on': {$gte: search}}, (function (err, count) {
            if (err) {
                res.json({
                    status: 'ERROR',
                    message: err,
                });
            }
            res.json({
                activeClients: count
            });
        }));
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

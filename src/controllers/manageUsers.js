/**
 * Acuparse Release Management Service
 * @copyright Copyright (C) 2015-2022 Maxwell Power
 * @author Maxwell Power <max@acuparse.com>
 * @link http://www.acuparse.com
 * @license MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * File: controllers/manageClients.js
 * Admin API Controller
 */

// Import Databases
DB_secrets = require('../models/secrets');

const auth = require('../includes/authenticate');

function makeid(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

// Add User
exports.addUser = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_secrets.exists({'username': req.body.username}, function (err, result) {
            if (!result) {
                const secrets = new DB_secrets();
                secrets.username = req.body.username;
                secrets.secret = makeid(32);
                secrets.save(function (err) {
                    if (err)
                        res.json(err);
                    res.status(201).send({
                        status: 'SUCCESS',
                        message: 'Added User',
                        secrets
                    });
                    console.log('[ADMIN] Added User ' + req.body.username);
                });
            } else {
                res.status(409).send({
                        status: 'ERROR',
                        message: 'User Exists'
                    }
                );
            }
        })
    } else {
        res.status(401).send({
                status: 'ERROR',
                message: 'Unauthorized'
            }
        );
    }
}

// List All
exports.listUsers = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_secrets.find({}, '-_id -secret -__v', function (err, secrets) {
            if (err) {
                res.json({
                    status: 'ERROR',
                    message: err,
                });
            }
            res.json({
                secrets
            });
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// Delete user
exports.deleteUser = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_secrets.exists({'username': req.body.username}, function (err, result) {
            if (result) {
                DB_secrets.deleteMany({'username': req.body.username}, function (err) {
                    if (err) {
                        res.json({
                            status: 'ERROR',
                            message: err,
                        });
                    }
                    res.json({
                        status: 'SUCCESS',
                        message: 'Removed ' + req.body.username
                    });
                    console.log('[ADMIN] Removed User ' + req.body.username);
                });
            } else {
                res.status(400).send({
                        status: 'ERROR',
                        message: 'User Does Not Exist'
                    }
                );
            }
        });
    } else {
        res.status(401).send({
                status: 'ERROR',
                message: 'Unauthorized'
            }
        );
    }
}

// View Total Count
exports.usersCount = async function (req, res) {
    if (await auth.authenticate(req.token)) {
        await DB_secrets.estimatedDocumentCount(function (err, count) {
            if (err) {
                res.json({
                    status: 'ERROR',
                    message: err,
                });
            }
            res.json({
                totalUsers: count
            });
        });
    } else {
        res.status(401).send({
            status: 'ERROR',
            message: 'Unauthorized'
        })
    }
}

// Add First User
exports.firstUser = async function (req, res) {
    await DB_secrets.countDocuments(function (err, count) {
        if (err) {
            res.json({
                status: 'ERROR',
                message: err,
            });
        } else if (count === 0) {
            const secrets = new DB_secrets();
            secrets.username = req.body.username;
            secrets.secret = makeid(32);
            secrets.save(function (err) {
                if (err)
                    res.json(err);
                res.status(201).send({
                    status: 'SUCCESS',
                    message: 'Added First User',
                    secrets
                });
                console.log('[ADMIN] Created First User ' + req.body.username);
            })
        } else {
            res.status(401).send({
                status: 'ERROR',
                message: 'Unauthorized'
            })
        }
    });
}

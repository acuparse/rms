/**
 * Acuparse Release Management Service
 * @copyright Copyright (C) 2015-2022 Maxwell Power
 * @author Maxwell Power <max@acuparse.com>
 * @link http://www.acuparse.com
 * @license MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * File: routes/manageClients.js
 * Clients Route Index
 */

// Initialize express router
const router = require('express').Router();

// Import controllers
const clients = require('../controllers/manageClients');

// Client Routes
router.route('/')
    .get(clients.list);
router.route('/active')
    .get(clients.listActive);
router.route('/edit')
    .get(clients.view)
    .delete(clients.delete)
    .put(clients.update)
    .patch(clients.update);
router.route('/edit/mac')
    .get(clients.viewMAC)
    .delete(clients.deleteMAC);
router.route('/edit/clean')
    .delete(clients.deleteOLD);
router.route('/edit/clean/all')
    .delete(clients.deleteALL);

// Export API routes
module.exports = router;

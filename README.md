# Acuparse Release Management Service

Provides a lightweight service for clients to check for updates store basic data about the install base.

## Public Routes

### Default URL

- `curl -X GET /`

### Update Checking Route

- Update Checking and Telemetry Collection
  - `curl -X POST -H 'Content-Type: application/json' -d '{"chassis":"<CHASSIS>","virtualization":"<VIRT>","operating_system":"<OS>>","kernel":"<KERNEL>","architecture":"<ARCH>","clientID":"<CLIENTID>","version":"<VERSION>","mac":"<MAC>"}'`

### Client/Install View Route

- `curl -X GET /view/<clientID>`

### Healthcheck

- `curl -X GET /healthcheck`

## Private Routes

### User Management Route

- List
  - `curl -X GET /users`
- Add
  - `curl -X POST -d username=<username> /users`
- Delete
  - `curl -X DELETE -d username=<username> /users`
- Count
  - `curl -X GET /users/count`
    
### Release Management Route

- Latest Published Release
  - `curl -X GET /current`
- List
  - `curl -X GET /releases`
- Add
  - `curl -X POST -d version=<version> /releases`
- Delete
  - `curl -X DELETE -d version=<version> /releases`

### Client/Install Management Route

- List All
  - `curl -X GET /manage`
- List Active
  - `curl -X GET /manage/active`  
- View
  - `curl -X POST -d 'client=<clientID>' /manage/edit`
- View MAC
  - `curl -X GET -d 'mac=<mac>' /manage/edit/mac`  
- Update
  - `curl -X PUT -d 'client=<clientID>' /manage/edit`
- Delete
  - `curl -X DELETE -d 'client=<clientID>' /manage/edit`
- Delete MAC
  - `curl -X DELETE -d 'mac=<mac>' /manage/edit/mac`
- Delete Old
  - `curl -X DELETE /manage/edit/clean`
- Delete ALL
  - `curl -X DELETE /manage/edit/clean/all`

### Total Client/Install Count

Total
- `curl -X GET /count`

Active
- `curl -X GET /count/active`

### System Monitor

- `curl -X GET /sysmon`
